import React, { useState} from 'react';
import DataContext from "./src/context/DataContext";

import Main from './src/screens/Main.screen';

export default function App() {
  const [data, setData] = useState([]);
  const contextValue = { data, setData };

  return (
    <DataContext.Provider value={contextValue}>
      <Main />
    </DataContext.Provider>
  );
};