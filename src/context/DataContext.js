import React from 'react';

const DataContext = React.createContext(
  {
    data: {
      youtube: [],
      fastlist: []
    },
    setData: () => {}
  }
);

export default DataContext;