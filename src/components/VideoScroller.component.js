import React, { useRef } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import YoutubePlayer from 'react-native-youtube-iframe';

export default function VideoScroller(props) {

  const { videolist } = props;
  const playerRef = useRef(null);

  return (
    <ScrollView>
    {
      videolist.map( (eaVideo, index) => (
        <View style={[styles.videoCard, index === 0 ? styles.videoCardFeatured : styles.videoCardDefault]} key={index}>
          <View style={styles.videoCardHeader}>
            <View>
              {videoCardTitle(index, eaVideo.title)}
            </View>
            <Text style={styles.videoCardDate}>{eaVideo.date}</Text>
          </View>
          <View style={styles.player}>
            <YoutubePlayer
              ref={playerRef}
              height={220}
              videoId={eaVideo.id}
              play={index === 0}
              volume={100}
              playbackRate={1}
              playerParams={{
                cc_lang_pref: "us",
                showClosedCaptions: true,
              }}
            />
          </View>
            <Text style={styles.videoText}>{eaVideo.title}</Text>
        </View>
      ))
    }
    </ScrollView>
  );
};

const videoCardTitle = (index, title) => {
  if (index === 0) {
    return <Text style={styles.videoCardTitle}>Featured</Text>
  } else {
    return <Text style={styles.videoCardTitle}>Previous</Text>
  }
};

const styles = StyleSheet.create({
  videoCard: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginBottom: 6,
  },
  videoCardDefault: {
    backgroundColor: '#ced1d3',
  },
  videoCardFeatured: {
    backgroundColor: '#ced1d3',
  },
  videoCardHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    color: '#000000',
    fontSize: 14,
    paddingBottom: 5,
  },
  videoCardTitle: {
    fontFamily: 'Transformers',
    letterSpacing: .5,
  },
  videoCardDate: {
    fontSize: 10,
    fontFamily: 'Transformers',
    letterSpacing: .5,
  },
  videoText: {
    color: '#000000',
    alignSelf: 'center',
    fontSize: 12,
  },
  player: {
    alignSelf: 'stretch',
  }
});