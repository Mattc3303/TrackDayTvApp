import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image, Animated, Dimensions } from 'react-native'

import Swipeable from 'react-native-gesture-handler/Swipeable';

export default function VehicleCard(props) {

  const { id, make, model, year, mod, horse_power, torque, weight, engine, thumbnail, updateSelectedVehicle, navigation } = props;

  const [vehicleCardSelected, setVehicleCardSelected] = useState(false);

  let swipeableDom = null;

  const onVehiclePress = () => {
    navigation.navigate('vehicleDetail', {
      id: id
    });

  }

  useEffect(() => {
    if (vehicleCardSelected) {
      updateSelectedVehicle('add', id);
    } else {
      updateSelectedVehicle('remove', id);
    }
  }, [vehicleCardSelected]);

  // const renderLeftActions = (progress, dragX) => {
  //   const trans = dragX.interpolate({
  //     inputRange: [0, 50],
  //     outputRange: [-20, 0],
  //   });

  //   return (
  //     <View style={styles.leftAction}>
  //       <Animated.Text style={{transform: [{ translateX: trans }]}}>
  //       </Animated.Text>
  //     </View>
  //   );
  // };

  const onSwipeableOpen = () => {
    swipeableDom.close()
  }

  const longText = () => {
    const firsthalf = engine.substring(0, 25)
    const secondhalf = engine.substring(25, engine.length)

    if (engine.length > 20) {
      return (
        <View style={{flexDirection: 'column'}}>
          <Text style={styles.vehicleDetailsInfo}>{firsthalf}</Text>
          <Text style={styles.vehicleDetailsInfo}>{secondhalf}</Text>
        </View>
      )
    } else {
      return <Text style={styles.vehicleDetailsInfo}>{engine}</Text>
    }
  }

  return (
    <View>
      {/* <Swipeable ref={(swipeable => {swipeableDom = swipeable})} renderLeftActions={renderLeftActions} onSwipeableOpen={onSwipeableOpen}> */}
        <TouchableOpacity style={[styles.vehicleCard, vehicleCardSelected ? styles.vehicleCardSelected : null]} onPress={onVehiclePress}>
            <Image 
              style={styles.vehicleThumbnail}
              source={{uri: thumbnail}}
            />
            <View>
              <View style={styles.vehicleDetails}>
                <Text style={styles.vehicleDetailsCategory}>Make: </Text>
                <Text style={styles.vehicleDetailsInfo}>{make}</Text>
              </View>
              <View style={styles.vehicleDetails}>
                <Text style={styles.vehicleDetailsCategory}>Model: </Text>
                <Text style={styles.vehicleDetailsInfo}>{model}</Text>
              </View>
              <View style={styles.vehicleDetails}>
                <Text style={styles.vehicleDetailsCategory}>Year: </Text>
                <Text style={styles.vehicleDetailsInfo}>{year}</Text>
              </View>
              <View style={styles.vehicleDetails}>
                <Text style={styles.vehicleDetailsCategory}>Mod: </Text>
                <Text style={styles.vehicleDetailsInfo}>{mod}</Text>
              </View>
              <View style={styles.vehicleDetails}>
                <Text style={styles.vehicleDetailsCategory}>HP: </Text>
                <Text style={styles.vehicleDetailsInfo}>{horse_power} </Text>
                <Text style={styles.vehicleDetailsCategory}>Torque: </Text>
                <Text style={styles.vehicleDetailsInfo}>{torque}</Text>
              </View>
              <View style={styles.vehicleDetails}>
                <Text style={styles.vehicleDetailsCategory}>Curb Weight: </Text>
                <Text style={styles.vehicleDetailsInfo}>{weight}</Text>
              </View>
              <View style={styles.vehicleDetails}>
                <Text style={styles.vehicleDetailsCategory}>Engine Type: </Text>
                {longText()}
              </View>
            </View>
        </TouchableOpacity>
      {/* </Swipeable> */}
    </View>
  )
}

const styles = StyleSheet.create({
  vehicleCard: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginBottom: 7,
    backgroundColor: 'rgba(206, 209, 211, 1)',
    flexDirection: 'row',
    width: Dimensions.get('screen').width,
  },
  vehicleCardSelected: {
    backgroundColor: '#ffa500',
  },
  vehicleThumbnail: { 
    height: 120,
    width: 160,
    marginRight: '5%',
  },
  vehicleDetails: {
    flexDirection: 'row', 
    flex: 1,
  },
  vehicleDetailsCategory: {
    fontSize: 11,
    fontWeight: 'bold',
  },
  vehicleDetailsInfo: {
    fontSize: 11,
  },
  leftAction: {
    width: '10%',
  }
});