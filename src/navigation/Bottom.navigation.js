import React from 'react';
import { StyleSheet, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';

import Home from '../screens/Home.screen';
import FastList from '../screens/Fastlist.screen';
import About from '../screens/About.screen';

const Tab = createBottomTabNavigator()

function getHeaderTitle(route) {
  
  const routeName = route.state?.routes[route.state.index]?.name;

  switch (routeName) {
    case 'Home':
      return 'Home';
    case 'FastList':
      return 'Fast List';
    case 'About':
      return 'About';
  }
}

export default function BottomNavigation({ navigation, route }) {
  navigation.setOptions({ headerTitle: getHeaderTitle(route) });
  return (
    <Tab.Navigator
      style={styles.tabContainer}
      initialRouteName="Home"
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          switch (route.name) {
            case 'Home':
              iconName = 'md-home';
              break;
            case 'FastList':
              iconName = 'md-clipboard';
              break;
            case 'About':
              iconName = 'ios-information-circle';
              break;
            default:
              iconName = '';
          }

          return (
            <View style={styles.icon}>
              <Ionicons name={iconName} size={32} color={color} />
            </View>
          )
        },
        headerTitleStyle: {
          fontFamily: 'Transformers',
        }
      })}
      tabBarOptions={{
        activeTintColor: '#ffa500',
        inactiveTintColor: '#ffffff',
        showLabel: false,
        style: {
          backgroundColor: '#3A3C3E',
        }
      }}
    >
      <Tab.Screen
        name="Home"
        component={Home}
      />
      <Tab.Screen
        name="FastList"
        component={FastList}
      />
      <Tab.Screen
        name="About"
        component={About}
      />
    </Tab.Navigator>
  )
}

const styles = StyleSheet.create({
  tabContainer: {
    flex: 1,
  },
  icon: {
    alignSelf: 'center',
    color: '#ffffff',
  }
})
