import React, { useState, useEffect, useContext } from 'react';
import { View, StyleSheet } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import * as Font from 'expo-font';

import { initializeFirebase } from '../api/firebase.api';
import { getFastList } from '../api/firebase.api';
import { getYTPlaylist } from '../api/youtube.api';
import theme from '../constants/Theme';

// Components...
import Intro from './Intro.screen';
import { NavigationContainer } from '@react-navigation/native';
import BottomNavigation from '../navigation/Bottom.navigation';
import DataContext from "../context/DataContext";

// Third Party...
import moment from 'moment';

const Stack = createStackNavigator();

export default function Main() {

  const [loading, setLoading] = useState(false);
  const { data, setData } = useContext(DataContext);
  const [fontsLoaded, setFontsloaded] = useState(false);

  useEffect(() => {
    (async () => {
      await Font.loadAsync({
        'Transformers': require('../../assets/Transformers.ttf'),
      }).then(() => setFontsloaded(true));
    })();
    console.log(fontsLoaded)
  }, [])

  useEffect(() => {
    const start = new Date().getTime();
    let end = null;

    initializeFirebase();

    getYTPlaylist()
      .then(YTList => {
        const videoList = YTList.map(eaVideo => {
          const video = eaVideo.snippet;
          const videoTitle = video.title;
          const videoPublishedAt = moment(video.publishedAt).format('MMMM Do YYYY');
          const videoId = video.resourceId.videoId;

          return {
            title: videoTitle, id: videoId, date: videoPublishedAt
          }
        });

        setData(prevState => ({
          ...prevState,
          youtube: videoList
        }));

      })

    getFastList().on('value', data => {
      setData(prevState => ({
        ...prevState,
        fastlist: data.val().fastlist
      }));

      end = new Date().getTime(); 
      const totalTime = (end - start) / 1000;
      
      if (totalTime < 5) {
        // const timer = setTimeout(() => { setLoading(true); }, 9000);
        // return () => clearTimeout(timer);
        setLoading(true);
      } else {
        setLoading(true);
      }

    });
  }, []);

  return (
    <View style={styles.container}>
      {
        (fontsLoaded && loading)
          ? <NavigationContainer>
              <Stack.Navigator screenOptions={{headerStyle: { backgroundColor: '#3A3C3E', height: 60}}}>
                <Stack.Screen 
                  name="Home" 
                  component={BottomNavigation} 
                  options={{
                    headerTitleStyle: {
                      color: theme.COLORS.THEME_ORANGE,
                    },
                  }}
                />
              </Stack.Navigator>
            </NavigationContainer>
          : <Intro />
      }
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3A3C3E',
  }
});
