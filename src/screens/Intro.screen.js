import * as React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'

export default function Intro() {
  return (
    <View style={styles.container}>
      <View>
        <Image
          style={styles.logo}
          source={require('../../assets/intro_logo.png')}
        />
        <Text style={styles.logoText}>TrackDay</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#000000',
  },
  logo: {
    width: 500,
    height: 500,
    marginTop: 60,
  },
  logoText: {
    color: '#ffa500',
    alignSelf: 'center',
    fontSize: 30,
    letterSpacing: 2,
    fontWeight: 'bold',
    marginTop: -160,
  }
});