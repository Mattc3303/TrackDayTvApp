import React, { useContext } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import DataContext from '../context/DataContext';

import VideoScroller from '../components/VideoScroller.component';

export default function Home() {

  const videolist = useContext(DataContext).data.youtube;

  return (
    <View style={styles.container}>
      { videolist && <VideoScroller videolist={videolist} /> }
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#8f9396',
    paddingVertical: 10,
    paddingHorizontal: 4,
    flex: 1,
  }
});