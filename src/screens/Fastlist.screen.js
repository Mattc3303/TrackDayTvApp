import React from 'react';
import { StyleSheet, View } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

import VehicleScroller from './fastlist/VehicleScroller.screen';
import VehicleDetail from './fastlist/VehicleDetail.screen';
import VehicleCompare from './fastlist/VehicleCompare.screen';

const Stack = createStackNavigator();

export default function FastList() {
  return (
    <View style={styles.container}>
      <Stack.Navigator
        initialRouteName='vehicleScroller'
        screenOptions={{
          headerStyle: { height: 1 },
          headerTitleContainerStyle: { margin: 0, padding: 0}
        }}
      >
        <Stack.Screen name='vehicleScroller' component={VehicleScroller} options={{headerShown:false}} />
        <Stack.Screen 
          name='vehicleDetail' 
          component={VehicleDetail} 
          options={{headerShown:false}}
        />
        <Stack.Screen name='vehicleCompare' component={VehicleCompare} />
      </Stack.Navigator>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#8f9396',
    paddingVertical: 4,
    paddingHorizontal: 4,
    flex: 1,
  },
});