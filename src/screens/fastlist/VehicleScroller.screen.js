import React, { useEffect, useState, useContext } from 'react';
import { StyleSheet, View, Text, ScrollView, TouchableOpacity, Dimensions } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';

import DataContext from '../../context/DataContext';
import VehicleCard from '../../components/VehicleCard.component';

export default function VehicleScroller({navigation}) {

  const fastlist = useContext(DataContext).data.fastlist;

  const [currentFastlist, setCurrentFastlist] = useState([]);
  const [makeList, setMakeList] = useState([]);

  const [arr, setArr] = useState([]);

  useEffect(() => {
    setCurrentFastlist(fastlist);

    const makeListFiltered = fastlist
      .map(eaVehicle => eaVehicle.make)
      .filter( (eaVehicle, index, self) => self.indexOf(eaVehicle) === index)
      .map( eaVehicle => ({label: eaVehicle, value: eaVehicle}) );

    makeListFiltered.unshift({label: 'SHOW ALL', value: 'show_all'});
    setMakeList(makeListFiltered);
  }, [])

  const updateFastlist = (selectedMake) => {
    if (selectedMake.value === 'show_all') {
      setCurrentFastlist(fastlist);
    } else {
      const filteredFastlist = fastlist.filter(eaVehicle => eaVehicle.make === selectedMake.value);
      setCurrentFastlist(filteredFastlist);
    }
  }

  // TODO: This needs work find a way to get the lastest updated state
  function updateSelectedVehicle(status, key) {
    if (status === 'add') {
      setArr(arr => ([...arr, ...[key]]));
    }
    if (status === 'remove') {
      const arrClone = arr.filter(item => item !== key);
      setArr(() => arrClone)
    }
  }

  const launchCompareScreen = () => {

  }

  return (
    <View style={styles.vehicleScrollerContainer}>
      <DropDownPicker
        items={makeList}
        placeholder='Make'
        searchable={true}
        searchablePlaceholder='Search for Make'
        containerStyle={{ height: 40, paddingBottom: 5 }}
        dropDownMaxHeight={parseInt(Dimensions.get('window').height)}
        dropDownStyle={{opacity: .9}}
        itemStyle={{borderBottomColor: '#ced1d3', borderBottomWidth: 1}}
        activeLabelStyle={{ color: '#ffa500' }}
        searchableStyle={{ borderBottomColor: '#000000', borderBottomWidth: 2 }}
        onChangeItem={selectedMake => updateFastlist(selectedMake)}
      />
      <ScrollView style={styles.vehicleScrollContainer}>
        {
          currentFastlist.map((eaVehicle, index) => {

            const { id, make, model, year, mod, horse_power, torque, weight, engine, thumbnail } = eaVehicle;

            return (
              <VehicleCard 
                key={index}
                id={id}
                thumbnail={thumbnail}
                make={make}
                model={model}
                year={year}
                mod={mod}
                horse_power={horse_power}
                torque={torque}
                weight={weight}
                engine={engine}
                updateSelectedVehicle={updateSelectedVehicle}
                arr={arr}
                navigation={navigation}
              />
            )
          })
        }
      </ScrollView>
      <View style={styles.bottomBar}>
        <TouchableOpacity 
          style={styles.compareButton}
          onPress={launchCompareScreen}
        >
          <Text style={{fontWeight: 'bold'}}>Compare Cars</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  vehicleScrollerContainer: {
    flex: 1,
    backgroundColor: '#8f9396',
  },
  bottomBar: {
    position: 'absolute',
    top: parseInt(Dimensions.get('window').height) - 170,
    width: '100%',
    alignItems: 'center',
  },
  compareButton: {
    height: 40,
    width: '60%',
    backgroundColor: '#ffa500',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    opacity: .8,
    display: 'none',
  }
});