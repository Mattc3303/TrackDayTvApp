import React, { useState, useEffect, useContext } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image, ScrollView, Dimensions } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import DataContext from '../../context/DataContext';
import theme from '../../constants/Theme';

export default function VehicleDetail({ route, navigation }) {

  const { id } = route.params;
  const fastlist = useContext(DataContext).data.fastlist;

  const [currentVehicle, setCurrentVehicle] = useState([]);

  useEffect(() => {
    const getVehicle = fastlist.filter(eaVehicle => eaVehicle.id === id)[0];
    setCurrentVehicle(getVehicle);
  }, []);

  const onBack = () => {
    navigation.navigate('vehicleScroller');
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.header} onPress={onBack}>
        <Ionicons name={'md-arrow-back'} size={22} color={'#000000'} />
        <Text style={styles.headerText}>Back</Text>
      </TouchableOpacity>

      <View style={styles.vehicleThumbnailContainer}>
        <Image
          style={styles.vehicleThumbnail}
          source={{ uri: currentVehicle.thumbnail }}
        />
      </View>

      <ScrollView style={styles.vehicleContainer}>
        <View style={styles.infoSection}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>Make: </Text><Text>{currentVehicle.make}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>Model: </Text><Text>{currentVehicle.model}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>Year: </Text><Text>{currentVehicle.year}</Text>
          </View>
        </View>

        <View style={styles.timeSection}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>40 - 100: </Text>
            <Text style={{ color: theme.COLORS.THEME_ORANGE }}>{currentVehicle.time_40_100}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>60 - 130: </Text>
            <Text style={{ color: theme.COLORS.THEME_ORANGE }}>{currentVehicle.time_60_130}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>Half Mile: </Text>
            <Text style={{ color: theme.COLORS.THEME_ORANGE }}>{currentVehicle.half_mile}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>Half Mile ET: </Text>
            <Text style={{ color: theme.COLORS.THEME_ORANGE }}>{currentVehicle.half_mile_et}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>Quarter Mile: </Text>
            <Text style={{ color: theme.COLORS.THEME_ORANGE }}>{currentVehicle.quarter_mile}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>Quarter Mile: </Text>
            <Text style={{ color: theme.COLORS.THEME_ORANGE }}>{currentVehicle.quarter_mile_et}</Text>
          </View>
        </View>

        <View style={styles.infoSection}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>Type: </Text><Text>{currentVehicle.type}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>MSRP: </Text><Text>${currentVehicle.msrp}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>Engine: </Text><Text>{currentVehicle.engine}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>Horse Power: </Text><Text>{currentVehicle.horse_power}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>Torque: </Text><Text>{currentVehicle.torque}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>Transmission: </Text><Text>{currentVehicle.transmission}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>Weight: </Text><Text>{currentVehicle.weight}</Text>
          </View>
        </View>

        <View style={styles.modSection}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>Mods: </Text><Text>{currentVehicle.mod}</Text>
          </View>
        </View>

      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.COLORS.BACKGROUND,
    paddingHorizontal: 4,
  },
  header: {
    paddingVertical: 2,
    flexDirection: 'row',
  },
  headerText: {
    paddingLeft: 8,
  },
  vehicleThumbnailContainer: {
    height: Dimensions.get('window').width * 0.6,
    width: Dimensions.get('window').width,
  },
  vehicleThumbnail: {
    flex: 1,
    height: undefined,
    width: undefined
  },
  infoSection: {
    marginTop: 10,
    paddingVertical: 6,
    paddingLeft: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#000000',
  },
  modSection: {
    paddingVertical: 6,
    paddingLeft: 10,
  },
  timeSection: {
    paddingVertical: 6,
    paddingLeft: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#000000',
  },
});