import axios from 'axios'

// Params: number
export async function getYTPlaylist() {
  return axios(
    `https://www.googleapis.com/youtube/v3/playlistItems?key=AIzaSyAc7G-I32ArfL2a1cf_JQQJ8DYYVjc7fgE&playlistId	=PLhpeo4byMku99DOfUkNNUWnZvjxxsv9pE&part=snippet&maxResults=2`
  ).then( result => result.data.items)
}