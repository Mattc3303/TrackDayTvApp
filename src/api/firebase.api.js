import * as firebase from 'firebase'

// Optionally import the services that you want to use
import "firebase/database"
import "firebase/firestore"

export function initializeFirebase() {
  // Initialize Firebase
  const firebaseConfig = {
    apiKey: "AIzaSyBbDmGwan5mElRwDtNX4moemxlgqJneLOY",
    authDomain: "trackdaytv-ae858.firebaseapp.com",
    databaseURL: "https://trackdaytv-ae858.firebaseio.com/",
    projectId: "trackdaytv-ae858",
    appId: "app-id",
  }

  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig)
  }
}

export function getFastList() {
  const database = firebase.database()
  const fastlist = database.ref('1Q1i3oYUaQHx_rngxSzblVHRG4flj3a36fI8VhI2D0bg')
  return fastlist
}

