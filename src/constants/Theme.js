export default {
  COLORS: {
    BACKGROUND: '#8f9396',
    PRIMARY_FONT: '#000000',
    THEME_ORANGE: '#ffa500',
    ORANGE_DARK: '#d88c00'
  },
  FONTS: {
    FAMILY: 'Transformers',
  }
}